package br.com.thiago.test;

import br.com.thiago.servicos.Servicos;
import br.com.thiago.test.entidate.ColorResource;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;

public class ColorEndpointTest {

    @Before
    public void setupApi() {
        RestAssured.baseURI = "https://reqres.in/";
    }

    @Test
    public void getSingle() {
        RestAssured.given()
            .when()
                .get(Servicos.colorsResource.getValor(), 6)
            .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.SC_OK)
                .and()
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("Schemas/colorSingle.json"));
    }

    @Test
    public void getCollection() {
        RestAssured.given()
            .when()
                .get(Servicos.colorsCollectionPaginated.getValor(), 2)
            .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.SC_OK)
                .and()
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("Schemas/colorCollection.json"));
    }

    @Test
    public void createColor() {
        final ColorResource colorResource = new ColorResource(
                "novo fuchsia",
                2020,
                "#202001",
                "18-2020"
        );
        final ColorResource as = RestAssured
                .given()
                    .contentType(ContentType.JSON)
                .body(colorResource)
                .when()
                    .post(Servicos.colorsCollection.getValor())
                .then()
                    .statusCode(HttpStatus.SC_CREATED)
                    .extract().response().as(ColorResource.class);

        Assert.assertNotNull(as);
        Assert.assertNotNull(as.getId());
        Assert.assertNotNull(as.getCreatedAt());
        Assert.assertEquals(colorResource.getName(), as.getName());
        Assert.assertEquals(colorResource.getColor(), as.getColor());
        Assert.assertEquals(colorResource.getYear(), as.getYear());
        Assert.assertEquals(colorResource.getPantoneValue(), as.getPantoneValue());
    }

    @Test
    public void updateColor() {
        final ColorResource colorResource = new ColorResource();
        colorResource.setName("novo fuchsia");
        colorResource.setYear(2021);

        final ColorResource as = RestAssured.given()
                .body(colorResource)
            .when()
                .contentType(ContentType.JSON)
                .patch(Servicos.colorsResource.getValor(), 3)
            .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().response().as(ColorResource.class);

        Assert.assertNotNull(as.getUpdatedAt());
        Assert.assertNull(as.getColor());           // color fica null porque não foi modificado no patch
        Assert.assertNull(as.getPantoneValue());    // color fica null porque não foi modificado no patch
        Assert.assertEquals(colorResource.getName(), as.getName());
        Assert.assertEquals(colorResource.getYear(), as.getYear());
    }

    @Test
    public void deleteColor() {
        RestAssured.given()
            .when()
                .delete(Servicos.colorsResource.getValor(), 3)
            .then()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }

}
